package com.apijava8.teste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apijava8Application {

	public static void main(String[] args) {
		SpringApplication.run(Apijava8Application.class, args);
	}
}
