package com.apijava8.teste.ws.controller;

import java.util.Collection;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apijava8.teste.ws.service.ApiTesteService;

@RestController
public class ApiTesteController {

	private ApiTesteService service = new ApiTesteService();

	@RequestMapping(method = RequestMethod.GET, value = "/nomes", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<String>> obterTodos() {
		Collection<String> modelos = service.obterTodosNomes();
		return new ResponseEntity<>(modelos, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/nomes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> alterar(@RequestBody Object nome) {
		Collection<Object> modelos = service.alterarNome(nome);
		return new ResponseEntity<>(modelos, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/nomes/{id}")
	public ResponseEntity<Object> excluirPorId(@PathVariable Long id) {
		service.excluirNomePorId(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/nomes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> cadastrar(@RequestBody Object nome) {
		Collection<Object> nomes = service.cadastrarNome(nome);
		return new ResponseEntity<>(nomes, HttpStatus.CREATED);
	}

}
